import sys
import os
import shutil


def extract_images_to_one_folder(input_dir):
    #file structure is just finished jd_eXtract module
    for f in os.listdir(input_dir):
        each_f_dir = os.path.join(input_dir,f)
        try:
            os.system('rm -f %s'%(each_f_dir+'/info.txt') )
        except:
            pass
        output_dir = each_f_dir + '/faces'
        try:
            os.system('mkdir ' + output_dir)
        except:
            pass
        output_dir = output_dir + '/0/'
        try:
            os.system('mkdir ' + output_dir)
        except:
            pass
        count = 0
        face_list = []
        # like 0,1,2,3,etc..
        for sub_f in os.listdir(each_f_dir):
            images_dir = os.path.join(each_f_dir, sub_f)

            if sub_f[-3:] == 'jpg' or sub_f == 'faces':
                continue
            elif sub_f[-3:] == 'txt':
                print ('rm -f %s'% (images_dir) )
                try:
                    os.system('rm -f %s'% (images_dir) )
                    continue
                except:
                    pass

            images = os.listdir(images_dir)
            for im in images:
                im_dir = images_dir + '/' +im
                if im[:3] == 'dim' or im[:5] == 'clear' or im[-3:] == 'txt':
                    continue
                elif im[-3:] == 'jpg' :
                    try:
                        os.system('mv -f %s %s'%(im_dir,output_dir))
                    except:
                        pass
                    new_im_dir = os.path.join(output_dir,im)
                    face_list.append(new_im_dir)


            if sub_f != 'faces':
                shutil.rmtree(images_dir)
        with file(output_dir+ 'faces.list', "w") as fl:
            for line in face_list:
                fl.write(line + '\n')
                count += 1
        print(f,' ',count)


if __name__ == '__main__' :
    input_dir = '/home/root-26/workspaces/Project/baitian/FaceIdentification_TestSet/Cluster_dataset/2018_01_19_test_mtcnn'
    extract_images_to_one_folder(input_dir)