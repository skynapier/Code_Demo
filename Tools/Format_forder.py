import os
import shutil

def format(input_dir):
    '''
    file strucute is just filename( 0, 1, 2,3 ...)
    :param input_dir:
    :return:
    '''
    output_dir = os.path.join(input_dir , 'faces')
    try:
        os.system('mkdir ' + output_dir)
    except:
        pass
    output_dir = os.path.join(output_dir , '0')
    try:
        os.system('mkdir ' + output_dir)
    except:
        pass
    count = 0
    face_list = []
    for f in os.listdir(input_dir):
        person_dir = os.path.join(input_dir, f)
        if f[-3:] == 'jpg' or f == 'faces':
            continue
        elif f[-3:] == 'txt':
            print ('rm -f %s' % (person_dir))
            try:
                os.system('rm -f %s' % (person_dir))
                continue
            except:
                pass
        person_images = os.listdir(person_dir)
        for im in person_images:
            im_dir = person_dir + '/' + im
            if im[:3] == 'dim' or im[:5] == 'clear' or im[-3:] == 'txt':
                continue
            elif im[-3:] == 'jpg':
                try:
                    os.system('cp -f %s %s' % (im_dir, output_dir))
                except:
                    pass
                new_im_dir = os.path.join(output_dir, im)
                face_list.append(new_im_dir)
        if f != 'faces':
            shutil.rmtree(person_dir)
    print output_dir + '/faces.list'
    with file(output_dir + '/faces.list', "w") as fl:
        for line in face_list:
            fl.write(line)
            count += 1
    print(f, ' ', count)

if __name__ == '__main__' :
    input_dir = '/home/root-26/workspaces/Project/baitian/FaceIdentification_TestSet/Cluster_dataset/2018_01_19_test_mtcnn'
    format(input_dir)