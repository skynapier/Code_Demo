import os

def read_files():
    root_dir = "/home/root-26/workspaces/Project/baitian/FaceIdentification_TestSet/Cluster_dataset"

    for file_name in os.listdir(root_dir):
        file_dir = root_dir + "/" + file_name
        with file(file_dir + "/match_face_feature.txt" , "w" ) as mff:
            faces,features = [],[]
            for subfile_name in os.listdir(file_dir):
                if subfile_name.startswith("feature"):
                    feature_dir = file_dir + "/" + subfile_name
                    features = read_list(feature_dir)
                if subfile_name == 'faces':
                    faces_dir = file_dir + "/faces/faces.list"
                    faces = read_list(faces_dir)
            for face,feature in zip(faces,features):
                mff.write(face + "  " + feature + "\n")

def read_list(fn):
    ret = []
    with open(fn , 'r') as f:
        for line in f:
            ret.append(line.strip())
    return ret

read_files()