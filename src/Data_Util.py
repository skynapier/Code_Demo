import os


class Label_Data(object):
    '''
    Labeling Data
    '''
    label_data_dict = {}

    def __init__(self, label_dir):

        self.label_data_dict = {}

        cluster_list = []
        for sub_clus_dir in os.listdir(label_dir):
            # should like be ~/faces/01
            if sub_clus_dir[-4:] == 'list' or sub_clus_dir[-3:] == 'txt' :
                continue
            img_names_list = os.listdir(label_dir + "/" + sub_clus_dir)
            cluster = []

            for name in img_names_list:
                if name == 'center.png' or name[:5] == 'clear' or name[:3] == 'dim':
                    continue
                else:
                    cluster.append(name)
            cluster_list.append(cluster)
        for i in range(len(cluster_list)):
            self.label_data_dict[i] = cluster_list[i]



class Cluster_Result_Data(object):
    '''
    Clustering Results Data
    '''
    cluster_data_list = []

    def __init__(self, result_dir):

        self.cluster_data_list = []

        sub_clus_dirs = os.listdir(result_dir)

        for sub_clus_name in sub_clus_dirs:
            # Example "00055-203_2017-11-23-10-20-36"
            if sub_clus_name[:6] == 'centro':
                continue

            img_names_list = os.listdir(os.path.join(result_dir,sub_clus_name))
            cluster = []
            for name in os.listdir(os.path.join(result_dir,sub_clus_name)):
                if name == 'center.png':
                    continue
                else:
                    cluster.append(name)

            self.cluster_data_list.append(cluster)

class Cluster_Debug_Result_Data(object):
    '''
    save different phase of cluster opreation (0,1,2,3,4)
    e.g. cluster - 0 - kmeans - [][]
    '''
    debug_result_dict = {}
    def __init__(self, debug_result_folder_dir):

        self.debug_result_dict = {}

        sub_debug_clus_dirs = os.listdir(debug_result_folder_dir)

        for sub_debug_clus_dir in sub_debug_clus_dirs:

            debug_clus_folder_path =  debug_result_folder_dir + "/" + sub_debug_clus_dir
            debug_Cluster_Result_List = Cluster_Result_Data(debug_clus_folder_path).cluster_data_list
            self.debug_result_dict[sub_debug_clus_dir] = debug_Cluster_Result_List





