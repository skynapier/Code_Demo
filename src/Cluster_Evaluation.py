from Methods import *
import Data_Util as data_util
import os

def count_eachfolder(cluster, Label_Data_Dict):
    total_TP,predict_Positive = 0,0
    ret = []

    for i in cluster.cluster_data_list:
        total_TP += count_pairwise_TP(i, Label_Data_Dict.label_data_dict)
        predict_Positive += count_pairwise_Positive(i)

    Positive = count_pairwise_T(Label_Data_Dict.label_data_dict)
    FN = Positive - total_TP
    FP = predict_Positive - total_TP
    precision = float(count_Precision(total_TP, predict_Positive))
    recall = float(count_Recall(total_TP,FN))
    F = float(count_F(precision,recall))
    ret.append('Positive: %.f TP: %.f FN: %.f P_prime: %.f FP %.f \n'%(Positive,total_TP,FN,predict_Positive,FP) )

    ret.append('Precison: %.4f Recall: %.4f F: %.4f \n'%(precision,recall,F) )
    return ret

def count_each_phase_folder_TP(debug_clus,Label_Data_Dict):
    debug_clus_dict = debug_clus.debug_result_dict
    ret = []
    for k in debug_clus_dict:
        debug_result_list = debug_clus_dict[k]
        total_TP,total_Positive = 0,0
        for i in debug_result_list:
            total_TP += count_pairwise_TP(i, Label_Data_Dict.label_data_dict)
            total_Positive += count_pairwise_Positive(i)

        precision = count_Precision(total_TP, total_Positive)
        ret.append('Name: %s \n'%(str(k)) )
        ret.append('Total_TP %.f Total_Positive %.f Precision %.4f \n'%(total_TP,total_Positive,precision) )

    return ret

def main(working_dir):
    for sub_folder in os.listdir(working_dir):
        sub_folder_dir = os.path.join(working_dir,sub_folder)
        save_dir = os.path.join(sub_folder_dir,'Cluster_Result.txt')
        with file(save_dir,'w') as result:
            result.write('Filename:  %s \n'%(str(sub_folder)) )
            print('Filename:  %s'%(str(sub_folder))  )
            clusters = []
            for folder in os.listdir(sub_folder_dir):

                if folder == 'cluster':
                    cluster_list_dir = os.path.join(sub_folder_dir,'cluster')
                    clus_list = data_util.Cluster_Result_Data(cluster_list_dir)
                    clusters.append( ('source output: ', clus_list ) )

                elif folder == 'faces':
                    label_data_dir = os.path.join(sub_folder_dir,'faces')
                    label_face = data_util.Label_Data(label_data_dir)

                elif folder == 'cluster-debug':
                    clus_debug_dir = os.path.join(sub_folder_dir,'cluster-debug')
                    for debug_fold in os.listdir(clus_debug_dir):
                        debug_clus_dir = os.path.join(clus_debug_dir,debug_fold)
                        debug_clus = data_util.Cluster_Result_Data(debug_clus_dir)
                        clusters.append( (debug_fold,debug_clus) )
                    # debug_clus_list = data_util.Cluster_Debug_Result_Data(clus_debug_dir)
            for i in clusters:
                name,clus = i[0],i[1]
                clus_result = count_eachfolder(clus,label_face)
                result.write(name+ ':\n')
                for line in clus_result:
                    result.write(line)
                    print(line)


if __name__ == '__main__':
    input_dir = '/home/root-26/workspaces/Project/baitian/FaceIdentification_TestSet/Cluster_dataset/2018_01_19_test_mtcnn_result/'
    main(input_dir)
