import math
from decimal import *

def binom(n,k):
    '''for count binomial coefficient "n choose k

    :param n:
    :param k:
    :return:
    '''
    result = 0
    if n == k:
        result = 1
    elif k == 1:
        result = n
    else:
        a = math.factorial(n)
        b = math.factorial(k)
        c = math.factorial(n-k)
        result =  a // (b * c)
    return result

def getKey (dct,value):
    '''get the key by the corresponding value if miss match then return None

    :param dct:
    :param value:
    :return:
    '''
    for k in dct:
        if isinstance(dct[k],list):
            if value in dct[k]:

                return k

        else:
            if value == dct[k]:

                return k

    return None

def count_pairwise_TP(result_list, label_dict):
    '''Use for count true positive pair for a target clusters(list)

    :param result_list:
    :param label_dict:
    :return:
    '''
    pair = 0
    if len(result_list) == 0:
        return 0
    elif len(result_list) == 1:
        return 1
    else:
        for i in range(len(result_list) - 1):
            for j in range(i+1, len(result_list)):
                i_Key = getKey(label_dict, result_list[i])
                j_Key = getKey(label_dict, result_list[j])
                if i_Key == j_Key and i_Key != None and j_Key != None:
                    pair += 1
        pair += len(result_list)

    return float(pair)

def count_pairwise_Positive(result_list):
    '''Count positive for each cluster which is (len of list) choose 2

    :param result_list:
    :return:
    '''
    if len(result_list) == 1:
        return 1
    elif len(result_list) == 0:
        return None
    else:
        return binom(len(result_list), 2) + len(result_list)

def count_pairwise_T(label_dict):
    ''' Use for count true pair

    :param label_dict:
    :return:
    '''

    T = 0
    for k in label_dict:
        each_label_list = label_dict[k]
        numOflabel_list = len(each_label_list)
        if numOflabel_list == 1:
            T += 1
        else:
            T += binom(numOflabel_list,2) + numOflabel_list

    return float(T)

def count_Precision(total_TP, total_Positive):
    '''Use for count precision P = TP / (TP + FP)

    :param total_TP:
    :param total_Positive:
    :return:
    '''
    return float(total_TP) / float(total_Positive)


def count_Recall(total_TP, FN):
    '''Use for count recall R = TP / (TP + FN)

    :param total_TP:
    :param FN:
    :return:
    '''
    return float(total_TP) / float(total_TP + FN)

def count_F(P,R):
    '''F = 2 * (P * R) / (P + R)

    :param P:
    :param R:
    :return:
    '''
    F = float(2 * float(P * R) / float(P + R))
    return '%.4f' % F





